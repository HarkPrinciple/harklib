using System.Security.Cryptography;
using System.Numerics;
using System.IO;
using System;

namespace HarkLib.Core
{
    [Serializable]
    public class RandomUIDManager : UIDManager
    {
        public RandomUIDManager(int size = 16)
            : base()
        {
            this.Size = size;
        }

        public int Size
        {
            get;
            private set;
        }

        private RNGCryptoServiceProvider _RNG = null;
        protected RNGCryptoServiceProvider RNG
        {
            get
            {
                if(_RNG == null)
                    _RNG = new RNGCryptoServiceProvider();
                
                return _RNG;
            }
        }

        public override BigInteger Reserve()
        {
            byte[] value = new byte[Size];
            RNG.GetBytes(value);
            return new BigInteger(value);
        }

        public override void Update(BigInteger value)
        { }
    }
}