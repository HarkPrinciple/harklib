using System;

namespace HarkLib.Core
{
    public class Processor
    {
        public static Random CreateRandom()
        {
            byte[] data = new byte[4];
            System.Security.Cryptography.RandomNumberGenerator.Create().GetBytes(data);
            return new Random(data.ToInt32());
        }
    }
}