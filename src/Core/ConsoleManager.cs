using System.Collections.Generic;
using System.Linq;
using System;

namespace HarkLib.Core
{
    public static class ConsoleManager
    {
        private static string PadValue(
            string value,
            int size = 30,
            int padding = 46,
            char separator = '|')
        {
            if(value.Length <= size)
                return value;
            
            string[] words = value.Split();
            string[] nwords = null;
            
            do
            {
                if(nwords != null)
                    words = nwords;
                nwords = words
                    .SelectMany(w => w.Length > size
                        ? new string[]
                        {
                            w.Substring(0, size),
                            w.Substring(size)
                        }
                        : new string[] { w }
                    ).ToArray();
            } while(words.Length != nwords.Length);

            List<string> values = new List<string>();

            string sentence = "";
            foreach(string word in words)
            {
                string tempSentence = (sentence + " " + word).Trim();
                if(tempSentence.Length > size)
                {
                    values.Add(sentence);
                    sentence = word;
                }
                else
                    sentence = tempSentence;
            }
            if(sentence.Length > 0)
                values.Add(sentence);
            
            return values
                .Aggregate((s1, s2) => s1 + Environment.NewLine + (separator + " " + s2).PadRight(size + 2).PadLeft(padding + size + 2))
                .Trim();
        }

        public static void DisplayHelp(
            string version,
            IDictionary<string, IDictionary<string, string>> values,
            string[] usages = null,
            IDictionary<string, IDictionary<string, string>> legends = null,
            int leftSize = 45)
        {
            usages = usages ?? new string[0];
            legends = legends ?? new Dictionary<string, IDictionary<string, string>>();

            Console.WriteLine();
            Console.WriteLine(HarkLib.Core.Hark.GetLogo(version));

            for(int i = 0; i < usages.Length; ++i)
            {
                if(i == 0)
                    Console.WriteLine(" Usage : {0}", usages[i]);
                else
                    Console.WriteLine("       : {0}", usages[i]);
            }
            Console.WriteLine();

            foreach(var kv in legends)
            {
                int maxLeft = kv.Value
                    .Select(x => x.Key)
                    .Select(str => str.Length)
                    .Max();

                Console.WriteLine(("[ " + kv.Key + " ]").PadLeftRight(maxLeft + 2, c : '='));

                foreach(var kv2 in kv.Value)
                    Console.WriteLine(" {0} - {1}", kv2.Key.PadRight(maxLeft), PadValue(kv2.Value, padding : maxLeft + 2, size : 76 - maxLeft - 1));
            }

            if(legends.Count > 0)
                Console.WriteLine();

            foreach(var kv in values)
            {
                Console.WriteLine(("[ " + kv.Key + " ]").PadLeftRight(leftSize + 1, c : '='));
                
                foreach(var kv2 in kv.Value)
                    Console.WriteLine(" {0}- {1}", kv2.Key.PadRight(leftSize), PadValue(kv2.Value, padding : leftSize + 1, size : 76 - leftSize - 1));
            }
        }

        public class Arguments
        {
            public Arguments(string[] args)
            {
                this.CommandPathNbValues = 0;
                this.ArgumentsKeyValue = new Dictionary<string, string>();
                this.ArgumentsKey = new Dictionary<string, bool>();
                this.Actions = new List<Action<Arguments>>();
                this.largs = new List<string>(args);
            }

            public static Arguments FromArguments(string[] args)
            {
                return new Arguments(args);
            }

            private List<string> largs;

            public Dictionary<string, string> ArgumentsKeyValue
            {
                get;
                private set;
            }
            public Dictionary<string, bool> ArgumentsKey
            {
                get;
                private set;
            }

            public List<Action<Arguments>> Actions
            {
                get;
                private set;
            }

            public Dictionary<string, string> CommandArguments
            {
                get;
                private set;
            }
            public int CommandPathNbValues
            {
                get;
                private set;
            }
            public string CommandPath
            {
                get;
                private set;
            }
            public Action<Arguments, Dictionary<string, string>> CommandExecution
            {
                get;
                private set;
            }

            public Action<Arguments> DefaultAction
            {
                get;
                private set;
            }

            public bool GetKey(string name)
            {
                return ArgumentsKey[name];
            }
            public string GetValue(string name)
            {
                return ArgumentsKeyValue[name];
            }

            public bool HasKey(string name)
            {
                return ArgumentsKey.ContainsKey(name);
            }
            public bool HasValue(string name)
            {
                return ArgumentsKeyValue.ContainsKey(name) && ArgumentsKeyValue[name] != null;
            }
            
            public Arguments ExtractValue(string name, Predicate<string> keyChecker, string defaultValue = null)
            {
                string value;
                ArgumentsKeyValue[name] = ConsoleManager.ExtractArg(largs, keyChecker, out value) ? value : defaultValue;
                return this;
            }
            public Arguments ExtractValue(string name, string key, string defaultValue = null)
            {
                string value;
                ArgumentsKeyValue[name] = ConsoleManager.ExtractArg(largs, key, out value) ? value : defaultValue;
                return this;
            }
            public Arguments ExtractValue(string name, string[] keys, string defaultValue = null)
            {
                string value;
                ArgumentsKeyValue[name] = ConsoleManager.ExtractArg(largs, keys, out value) ? value : defaultValue;
                return this;
            }

            public Arguments ExtractKey(string name, Predicate<string> keyChecker, bool value = true)
            {
                ArgumentsKey[name] = ConsoleManager.ExtractArg(largs, keyChecker) ? value : !value;
                return this;
            }
            public Arguments ExtractKey(string name, string key, bool value = true)
            {
                ArgumentsKey[name] = ConsoleManager.ExtractArg(largs, key) ? value : !value;
                return this;
            }
            public Arguments ExtractKey(string name, string[] keys, bool value = true)
            {
                ArgumentsKey[name] = ConsoleManager.ExtractArg(largs, keys) ? value : !value;
                return this;
            }

            public Arguments ExtractCommand(string command, Action<Arguments, Dictionary<string, string>> exec)
            {
                ExtractCommand(command.Split("/", StringSplitOptions.RemoveEmptyEntries), command, exec);
                return this;
            }
            public Arguments ExtractCommand(string[] commands, Action<Arguments, Dictionary<string, string>> exec)
            {
                foreach(string cmd in commands)
                    ExtractCommand(cmd, exec);
                return this;
            }
            public Arguments ExtractCommand(string command, string name = null)
            {
                ExtractCommand(command.Split("/", StringSplitOptions.RemoveEmptyEntries), name ?? command);
                return this;
            }
            public Arguments ExtractCommand(string[] commands, string name = null)
            {
                foreach(string cmd in commands)
                    ExtractCommand(cmd, name);
                return this;
            }
            private Arguments ExtractCommand(string[] commands, string name, Action<Arguments, Dictionary<string, string>> exec)
            {
                var cmdArgs = new Dictionary<string, string>();

                for(int i = 0; i < commands.Length; ++i)
                {
                    string cmd = commands[i].Trim();

                    if(cmd.StartsWith("{") && cmd.EndsWith("}"))
                    {
                        string argName = cmd.Substring(1, cmd.Length - 2);
                        if(argName.StartsWith("?"))
                        {
                            if(largs.Count <= i)
                                break;
                            argName = argName.Substring(1);
                        }
                        else
                        {
                            if(largs.Count <= i)
                                return this;
                        }
                        
                        if(argName.EndsWith("..."))
                            cmdArgs[argName.Substring(0, argName.Length - 3)] = largs
                                .Skip(i)
                                .Aggregate("", (s1, s2) => s1 + ' ' + s2)
                                .Trim();
                        else
                            cmdArgs[argName] = largs[i];
                    }
                    else
                    {
                        if(largs.Count <= i || largs[i].Trim().ToLower() != cmd.ToLower())
                            return this;
                    }
                }

                this.CommandPathNbValues = commands.Length;
                this.CommandArguments = cmdArgs;
                this.CommandExecution = exec;
                this.CommandPath = name ?? commands.Aggregate("", (s1,s2) => s1 + '/' + s2);

                return this;
            }

            public Arguments Default(Action<Arguments> action)
            {
                this.DefaultAction = action;
                return this;
            }

            public Arguments Before(Action<Arguments> action)
            {
                this.Actions.Add(action);
                return this;
            }

            public void Execute()
            {
                foreach(var action in Actions)
                    action(this);
                
                if(CommandExecution == null)
                    DefaultAction(this);
                else
                    CommandExecution(this, CommandArguments);
            }
        }
        
        public static bool ExtractArg(
            List<string> largs,
            string key)
        {
            string[] values;
            return ExtractArg(largs, key, 0, out values);
        }
        public static bool ExtractArg(
            List<string> largs,
            string key,
            out string value,
            bool caseSensitive = false)
        {
            return ExtractArg(largs, new string[] { key }, out value, caseSensitive);
        }
        public static bool ExtractArg(
            List<string> largs,
            string key,
            int nbValues,
            out string[] values,
            bool caseSensitive = false)
        {
            return ExtractArg(largs, new string[] { key }, nbValues, out values, caseSensitive);
        }

        public static bool ExtractArg(
            List<string> largs,
            string[] keys)
        {
            string[] values;
            return ExtractArg(largs, keys, 0, out values);
        }
        public static bool ExtractArg(
            List<string> largs,
            string[] keys,
            out string value,
            bool caseSensitive = false)
        {
            string[] values;
            bool result = ExtractArg(largs, keys, 1, out values, caseSensitive);
            value = result ? values[0] : null;
            return result;
        }
        public static bool ExtractArg(
            List<string> largs,
            string[] keys,
            int nbValues,
            out string[] values,
            bool caseSensitive = false)
        {
            return ExtractArg(
                largs,
                arg => caseSensitive ? keys.Any(k => k == arg) : keys.Any(k => arg.ToLower() == k.ToLower()),
                nbValues,
                out values
            );
        }
        
        public static bool ExtractArg(
            List<string> largs,
            Predicate<string> keyChecker)
        {
            string[] values;
            return ExtractArg(largs, keyChecker, 0, out values);
        }
        public static bool ExtractArg(
            List<string> largs,
            Predicate<string> keyChecker,
            out string value)
        {
            string[] values;
            bool result = ExtractArg(largs, keyChecker, 1, out values);
            value = result ? values[0] : null;
            return result;
        }
        public static bool ExtractArg(
            List<string> largs,
            Predicate<string> keyChecker,
            int nbValues,
            out string[] values)
        {
            values = null;

            for(int i = 0; i < largs.Count; ++i)
            {
                bool valid = keyChecker(largs[i]);
                
                if(valid)
                {
                    if(largs.Count <= i + nbValues)
                        throw new Exception("Invalid argument " + largs[i] + ".");
                    
                    values = new string[nbValues];
                    for(int j = 0; j < nbValues; ++j)
                        values[j] = largs[i + j + 1];
                    
                    largs.RemoveRange(i, nbValues + 1);
                    --i;
                }
            }

            return values != null;
        }
    }
}