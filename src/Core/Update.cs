using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.IO;
using System;

namespace HarkLib.Core
{
    public class Update
    {
        public string DownloadingString
        {
            get;
            set;
        }

        public string DownloadedString
        {
            get;
            set;
        }

        public string InstallingString
        {
            get;
            set;
        }

        public string UpdatedString
        {
            get;
            set;
        }

        public void Run(IDictionary<string, string> rfiles, int timeout = 1)
        {
            var files = new Dictionary<string, string>();

            using(var client = new WebClient())
            {
                foreach(var kv in rfiles)
                {
                    string file = Path.GetTempFileName();
                    files.Add(kv.Key, file);
                    Console.WriteLine(DownloadingString ?? " [ ] Downloading {0}...", kv.Key);
                    client.DownloadFile(kv.Value, file);
                    Console.WriteLine(DownloadedString ?? " [o] Downloaded {0}.", kv.Key);
                }
            }

            string cmdExtension = "cmd";
            string headerCmd = null;
            string copyCmd = null;
            string sleepCmd = null;
            string removeCmd = null;
            string endRemoveCmd = null;
            bool canOverwrite;

            switch(Environment.OSVersion.Platform)
            {
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.WinCE:
                    headerCmd = "@echo off";
                    copyCmd = "copy {0} {1}";
                    sleepCmd = "timeout {0}";
                    removeCmd = "del {0}";
                    endRemoveCmd = "start /b \"\" cmd /c del \"%~f0\"&exit /b";
                    canOverwrite = false;
                    break;

                default: // Unix + MacOSX
                    canOverwrite = true;
                    break;
            }

            string exeFolderPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            if(canOverwrite)
            {
                Console.WriteLine(InstallingString ?? " [ ] Installing in background...");

                foreach(var kv in files)
                {
                    string dest = Path.Combine(exeFolderPath, kv.Key);
                    if(File.Exists(dest))
                        File.Delete(dest);
                    
                    File.Move(kv.Value, dest);
                    File.Delete(kv.Value);
                }

                Console.WriteLine(UpdatedString ?? " [o] Updated.");
            }
            else
            {
                string execFile = Path.Combine(Guid.NewGuid().ToString() + "." + cmdExtension);

                using(Stream stream = File.Open(execFile, FileMode.Create))
                using(StreamWriter sw = new StreamWriter(stream))
                {
                    sw.WriteLine("{0}", headerCmd);
                    sw.WriteLine(sleepCmd, timeout);

                    foreach(var kv in files)
                        sw.WriteLine(copyCmd, kv.Value, Path.Combine(exeFolderPath, kv.Key));

                    foreach(var kv in files)
                        sw.WriteLine(removeCmd, kv.Value);

                    sw.WriteLine("echo {0}", UpdatedString ?? " [o] Updated.");
                    sw.WriteLine(endRemoveCmd, execFile);
                }

                Console.WriteLine(InstallingString ?? " [ ] Installing in background...");

                Console.WriteLine(" [i] Exec {0}", execFile);
                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = execFile;
                myProcess.StartInfo.CreateNoWindow = false;
                myProcess.Start();
            }
        }
    }
}