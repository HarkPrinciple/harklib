using System.Net;
using System;

namespace HarkLib.Core
{
    public static class CoreHelper
    {
        public static IPEndPoint ParseIPEndPoint(string endPoint)
        {
            string[] ep = endPoint.Split(':');
            if(ep.Length < 2)
                throw new FormatException("Invalid endpoint format");
            
            IPAddress ip;
            if(ep.Length > 2)
            {
                if(!IPAddress.TryParse(string.Join(":", ep, 0, ep.Length - 1), out ip))
                    throw new FormatException("Invalid ip-adress");
            }
            else
            {
                if(!IPAddress.TryParse(ep[0], out ip))
                    throw new FormatException("Invalid ip-adress");
            }

            int port;
            if(!int.TryParse(ep[ep.Length - 1], out port))
                throw new FormatException("Invalid port");
            
            return new IPEndPoint(ip, port);
        }
    }
}