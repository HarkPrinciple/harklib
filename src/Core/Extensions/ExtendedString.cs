using System.Collections.Generic;

namespace System
{
    public static class ExtendedString
    {
        public static byte[] GetBytes(this string str)
        {
            return System.Text.Encoding.UTF8.GetBytes(str);
        }
        
        public static int Count(this string str, string value)
        {
            int nb = 0;
            int index = -1;
            
            while((index = str.IndexOf(value, index + 1)) != -1)
                ++nb;
            
            return nb;
        }

        public static int IndexOfAny(
            this string str,
            string[] values,
            out int matchIndex,
            int start = 0,
            int count = -1)
        {
            count = count < 0 ? str.Length : count;
            int end = count + start;

            int[] offset = new int[values.Length];
            
            for(int i = start; i < end; ++i)
            {
                for(int j = 0; j < offset.Length; ++j)
                {
                    if(str[i] == values[j][offset[j]])
                    {
                        int newOffset = offset[j] + 1;
                        if(newOffset >= values[j].Length)
                        {
                            matchIndex = j;
                            return i;
                        }
                        ++offset[j];
                    }
                    else if(offset[j] > 0)
                        offset[j] = 0;
                }
            }

            matchIndex = -1;
            return -1;
        }
        
        public static string[] Split(this string str)
        {
            return str.Split((string[])null, StringSplitOptions.RemoveEmptyEntries);
        }
        public static string[] Split(this string str, string separator)
        {
            return str.Split(new string[] { separator }, StringSplitOptions.None);
        }
        public static string[] Split(this string str, string separator, StringSplitOptions options)
        {
            return str.Split(new string[] { separator }, options);
        }
        public static string[] Split(this string str, string separator, int nb)
        {
            return str.Split(new string[] { separator }, nb, StringSplitOptions.None);
        }
        public static string[] Split(this string str, string separator, int nb, StringSplitOptions options)
        {
            return str.Split(new string[] { separator }, nb, options);
        }
        
        public static List<string> SplitNotEscaped(
            this string str,
            char separator,
            char escapeChar = '\\',
            bool replaceEscaped = true,
            int nbMax = 0)
        {
            List<int> escapeFreeIndexes = new List<int>();
            List<string> strs = new List<string>();
            int lastIndex = 0;
            int index = 0;
            
            string doubleEscape = escapeChar.ToString() + escapeChar;
            string escapedStr = escapeChar.ToString() + separator;
            
            while((index = str.IndexOf(doubleEscape, index)) != -1)
            {
                index += 2;
                escapeFreeIndexes.Add(index);
            }
            
            index = 0;
            while((index = str.IndexOf(separator, index)) != -1)
            {
                if(nbMax > 0 && strs.Count >= nbMax - 1)
                    break;
                
                if(escapeFreeIndexes.Contains(index) || index == 0 || str[index - 1] != escapeChar)
                {
                    string newValue = str.Substring(lastIndex, index - lastIndex);
                    
                    if(replaceEscaped)
                        newValue = newValue.Replace(escapedStr, separator.ToString());
                    
                    strs.Add(newValue);
                    
                    lastIndex = index + 1;
                }
                
                ++index;
            }
            
            if(lastIndex != str.Length)
                strs.Add(str.Substring(lastIndex));
            
            return strs;
        }
        
        public static string Substring(this string str, string start, string end)
        {
            int startIndex = str.IndexOf(start) + start.Length;
            return str.Substring(startIndex, str.IndexOf(end) - startIndex);
        }
        public static string Substring(this string str, string start)
        {
            int startIndex = str.IndexOf(start) + start.Length;
            return str.Substring(startIndex);
        }
        public static string SubstringUntil(this string str, string end)
        {
            return str.Substring(0, str.IndexOf(end));
        }

        public static string PadLeftRight(this string str, int padding, char c = ' ', bool preferRight = false)
        {
            while(str.Length < padding)
                str = c + str + c;

            if(str.Length == padding)
                if(preferRight)
                    str = str + c;
                else
                    str = c + str;
            
            return str;
        }
    }   
}