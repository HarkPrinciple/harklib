using System.Collections.Generic;
using System.Linq;

namespace System.Linq
{
    public static class ExtendedLinq
    {
        public static IEnumerable<string> ToStrings<TSource>(this IEnumerable<TSource> source)
        {
            return source.Select(x => Convert.ToString(x));
        }
        
        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach(TSource x in source)
                action(x);
        }
        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource, int> action)
        {
            int i = 0;
            foreach(TSource x in source)
            {
                action(x, i);
                ++i;
            }
        }
        
        public static IEnumerable<TSource> Global<TSource>(this IEnumerable<TSource> source, Action<IEnumerable<TSource>> action)
        {
            action(source);
            return source;
        }
        
        public static IEnumerable<TSource> Peek<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            return source.Select(x => { action(x); return x; });
        }
        public static IEnumerable<TSource> Peek<TSource>(this IEnumerable<TSource> source, Action<TSource, int> action)
        {
            return source.Select((x,i) => { action(x, i); return x; });
        }
        
        public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> test)
        {
            return source.Select((x,i) => test(x,i)).All(b => b);
        }
        
        public static void Close<TSource>(this IEnumerable<TSource> source)
        {
            foreach(TSource x in source);
        }
        
        public static decimal Median<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, decimal> addOperation)
        {
            TSource[] temp = source.ToArray();    
            Array.Sort(temp);

            int size = temp.Length;
            if(size == 0)
                throw new InvalidOperationException("Empty collection");

            int halfSize = size / 2;
            if((size & 1) == 0)
            { // even
                TSource a = temp[halfSize - 1];
                TSource b = temp[halfSize];
                return addOperation(a, b) / 2m; // mean
            }
            else
            { // odd
                return (decimal)Convert.ChangeType(temp[halfSize], typeof(decimal));
            }
        }

        public static decimal Median(this IEnumerable<short> source)
        {
            return source.Median((a,b) => a + b);
        }
        public static decimal Median(this IEnumerable<byte> source)
        {
            return source.Median((a,b) => a + b);
        }
        public static decimal Median(this IEnumerable<long> source)
        {
            return source.Median((a,b) => a + b);
        }
        public static decimal Median(this IEnumerable<double> source)
        {
            return source.Median((a,b) => (decimal)(a + b));
        }
        public static decimal Median(this IEnumerable<float> source)
        {
            return source.Median((a,b) => (decimal)(a + b));
        }
        public static decimal Median(this IEnumerable<decimal> source)
        {
            return source.Median((a,b) => a + b);
        }
        public static decimal Median(this IEnumerable<char> source)
        {
            return source.Median((a,b) => a + b);
        }
        public static decimal Median(this IEnumerable<int> source)
        {
            return source.Median((a,b) => a + b);
        }
    }   
}