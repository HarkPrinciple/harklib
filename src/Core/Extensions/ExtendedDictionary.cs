using System.Collections.Generic;

namespace System
{
    public static class ExtendedDictionary
    {
        public static T2 GetOr<T1, T2>(this Dictionary<T1, T2> subject, T1 key, T2 defaultValue)
        {
            T2 value;
            return subject.TryGetValue(key, out value) ? value : defaultValue;
        }
    }
}