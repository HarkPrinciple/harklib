using System.Collections.Generic;

namespace System
{
    public static class ExtendedHex
    {
        private static uint[] _HexTable = null;
        public static uint[] HexTable
        {
            get
            {
                if(_HexTable == null)
                    _HexTable = CreateHexTable();
                return _HexTable;
            }
        }
        
        private static byte[] _ReverseHexTable = null;
        public static byte[] ReverseHexTable
        {
            get
            {
                if(_ReverseHexTable == null)
                    _ReverseHexTable = CreateReverseHexTable();
                return _ReverseHexTable;
            }
        }

        private static uint[] CreateHexTable()
        {
            var hexTable = new uint[256];
            for(int i = 0; i < 256; ++i)
            {
                string value = i.ToString("X2").ToLower();
                if(BitConverter.IsLittleEndian)
                    hexTable[i] = ((uint)value[0]) + ((uint)value[1] << 16);
                else
                    hexTable[i] = ((uint)value[1]) + ((uint)value[0] << 16);
            }
            return hexTable;
        }
        private static byte[] CreateReverseHexTable()
        {
            var hexTable = new byte[256];
            for(byte i = 0; i < 16; ++i)
            {
                if(i >= 10)
                {
                    hexTable['a' + (i - 10)] = i;
                    hexTable['A' + (i - 10)] = i;
                }
                else
                    hexTable['0' + i] = i;
            }
            
            return hexTable;
        }

        public static unsafe string ToHex(this byte[] data)
        {
            var hexTable = HexTable;
            var result = new string((char)0, data.Length << 1);

            fixed(char* pcresult = result)
            fixed(byte* pdata = data)
            {
                uint* presult = (uint*)pcresult;
                for(int i = 0; i < data.Length; ++i)
                    presult[i] = hexTable[pdata[i]];
            }

            return result;
        }
        public static unsafe byte[] FromHex(this string data)
        {
            var hexTable = ReverseHexTable;

            int nb = data.Length;
            int odd = (nb & 1) == 0 ? 0 : 1;

            var result = new byte[(nb + odd) >> 1];

            fixed(byte* presult = result)
            fixed(char* pcdata = data)
            {
                if(odd == 1)
                    presult[0] = (byte)(hexTable[pcdata[0]]);

                if(BitConverter.IsLittleEndian)
                {
                    for(int i = odd; i < data.Length; i += 2)
                        presult[(i >> 1) + odd] = (byte)(hexTable[pcdata[i + 1]] | (hexTable[pcdata[i]] << 4));
                }
                else
                {
                    for(int i = odd; i < data.Length; i += 2)
                        presult[(i >> 1) + odd] = (byte)(hexTable[pcdata[i]] | (hexTable[pcdata[i + 1]] << 4));
                }
            }

            return result;
        }
    }   
}