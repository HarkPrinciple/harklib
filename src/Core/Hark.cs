using System.Numerics;
using System;

namespace HarkLib.Core
{
    public static class Hark
    {
        public static string GetLogo(string version)
        {
            version = version ?? "1.0.0";
            version = version.Trim();

            if(version.Length > 5)
            {
                version = version.Substring(0, 5) + ']';
            }
            else if(version.Length < 5)
            {
                version += ']';
                while(version.Length < 6)
                    version += '=';
            }
            else
            {
                version += ']';
            }
            
            return ("=======================~~~~~~~~~~~~~~~~~~~~=======================\n" +
                "===                                                            ===\n" +
                "==   /[]    []\\      /[][][]\\     /[][][][][]\\    [][]   /[][]  ==\n" +
                "=   [][]\\  /[][]    /[]    []\\    [][]     [][]   [][] /[][]/    =\n" +
                "=~~~[][==oo==][]~~~/][]~~~~[][\\~~~[=o][][=o=]/~~~~[][=o=]~~~~~~~~=\n" +
                "=   [][]/  \\[][]   [][==oo==][]   [][] \\[][]\\     [][] \\[][]\\    =\n" +
                "==   \\[]    []/    [=o]    [o=]   [=o]    \\[][]   [=o]   \\[o=]  ==\n" +
                "===                                                            ===\n" +
                "=======================~~~~~~~~~~~~~~~~~~~~==============[v{version}=\n")
                .Replace("{version}", version);
        }
    }
}