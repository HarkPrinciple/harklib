using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

namespace HarkLib.Core
{
    public class DispatchStream : Stream
    {
        public DispatchStream(params Stream[] streams)
        {
            this.Streams = streams;
            this.Position = 0; // Needs 'Streams' to be set
        }
        
        public Stream[] Streams
        {
            get;
            private set;
        }

        public Stream CurrentStream
        {
            get
            {
                return Streams[Position];
            }
        }
        
        public override bool CanRead
        {
            get
            {
                return Streams.All(s => s.CanRead);
            }
        }
        
        public override bool CanSeek
        {
            get
            {
                return Streams.All(s => s.CanSeek);
            }
        }
        
        public override bool CanWrite
        {
            get
            {
                return Streams.All(s => s.CanWrite);
            }
        }
        
        public override long Length
        {
            get
            {
                return Streams
                    .Select(s => s.Length)
                    .Aggregate(0L, (i1,i2) => i1 + i2);
            }
        }
        
        private long _Position;
        public override long Position
        {
            get
            {
                return _Position;
            }
            set
            {
                _Position = value % Streams.Length;
            }
        }
        
        public override int ReadTimeout
        {
            get
            {
                return Streams
                    .Select(s => s.ReadTimeout)
                    .Min();
            }
            set
            {
                foreach(Stream stream in Streams)
                    stream.ReadTimeout = value;
            }
        }
        
        public override int WriteTimeout
        {
            get
            {
                return Streams
                    .Select(s => s.WriteTimeout)
                    .Min();
            }
            set
            {
                foreach(Stream stream in Streams)
                    stream.WriteTimeout = value;
            }
        }
        
        public override void Flush()
        {
            foreach(Stream stream in Streams)
                stream.Flush();
        }
        
        public override long Seek(long position, SeekOrigin origin)
        {
            return Streams
                .Select(s => s.Seek(position, origin))
                .Aggregate(0L, (i1,i2) => i1 + i2);
        }
        
        public override void SetLength(long length)
        {
            foreach(Stream stream in Streams)
                stream.SetLength(length);
        }

        private byte[] buffer = null;
        private int bufferIndex = 0;
        public override int Read(byte[] data, int start, int length)
        {
            if(buffer != null)
            {
                int nbBuffer = Math.Min(length, buffer.Length - bufferIndex);
                Array.Copy(buffer, bufferIndex, data, start, nbBuffer);
                bufferIndex += nbBuffer;

                if(bufferIndex == buffer.Length)
                    buffer = null;

                return nbBuffer;
            }

            int nb = CurrentStream.Read(4).ToInt32();

            if(length >= nb)
            {
                CurrentStream.Read(data, start, nb);
                ++this.Position;
                return nb;
            }

            buffer = new byte[nb];
            bufferIndex = length;
            CurrentStream.Read(buffer, 0, nb);

            Array.Copy(buffer, 0, data, start, length);

            ++this.Position;
            return length;
        }

        public override void Write(byte[] data, int start, int length)
        {
            CurrentStream.Write(length.GetBytes());
            CurrentStream.Write(data, start, length);

            ++this.Position;
        }

        public override void Close()
        {
            foreach(Stream stream in Streams)
                stream.Close();
        }
    }
}