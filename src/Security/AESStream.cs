using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

namespace HarkLib.Security
{
    public class AESStream : HarkLib.Core.BasedStream
    {
        public AESStream(Stream stream, byte[] key, byte[] iv)
             : base(stream)
        {
            this.Key = key;
            this.IV = iv;
        }

        private byte[] Key
        {
            get;
            set;
        }

        private byte[] IV
        {
            get;
            set;
        }

        public CryptoStream Writer
        {
            get;
            private set;
        }

        public CryptoStream Reader
        {
            get;
            private set;
        }
        
        public override int Read(byte[] data, int start, int length)
        {
            if(Reader == null)
                Reader = AES.GetDecrypter(Key, IV, BaseStream);
            return Reader.Read(data, start, length);
        }
        public override void Write(byte[] data, int start, int length)
        {
            if(Writer == null)
                Writer = AES.GetEncrypter(Key, IV, BaseStream);
            Writer.Write(data, start, length);
        }

        public override void Close()
        {/*
            if(Reader != null)
                Reader.Close();
            */
            if(Writer != null && !Writer.HasFlushedFinalBlock)
                Writer.FlushFinalBlock();
            
            BaseStream.Close();
        }
        
        public override void Flush()
        {
            if(Writer != null)
                Writer.Flush();
        }
    }
}