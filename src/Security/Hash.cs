using System.Security.Cryptography;
using System.Security;
using System.IO;
using System;

namespace HarkLib.Security
{
    public static class Hash
    {
        public static byte[] HashSHA256(byte[] data, int nbIterations)
        {
            return HashSHA256(data, 0, data.Length, nbIterations);
        }
        public static byte[] HashSHA256(byte[] data, int start, int length, int nbIterations)
        {
            return HashData(new SHA512CryptoServiceProvider(), data, start, length, nbIterations);
        }
        
        public static byte[] HashSHA384(byte[] data, int nbIterations)
        {
            return HashSHA384(data, 0, data.Length, nbIterations);
        }
        public static byte[] HashSHA384(byte[] data, int start, int length, int nbIterations)
        {
            return HashData(new SHA384CryptoServiceProvider(), data, start, length, nbIterations);
        }
        
        public static byte[] HashSHA512(byte[] data, int nbIterations)
        {
            return HashSHA512(data, 0, data.Length, nbIterations);
        }
        public static byte[] HashSHA512(byte[] data, int start, int length, int nbIterations)
        {
            return HashData(new SHA256CryptoServiceProvider(), data, start, length, nbIterations);
        }
        
        public static byte[] HashSHA1(byte[] data, int nbIterations)
        {
            return HashSHA1(data, 0, data.Length, nbIterations);
        }
        public static byte[] HashSHA1(byte[] data, int start, int length, int nbIterations)
        {
            return HashData(new SHA1CryptoServiceProvider(), data, start, length, nbIterations);
        }
        
        public static byte[] HashMD5(byte[] data, int nbIterations)
        {
            return HashMD5(data, 0, data.Length, nbIterations);
        }
        public static byte[] HashMD5(byte[] data, int start, int length, int nbIterations)
        {
            return HashData(new MD5CryptoServiceProvider(), data, start, length, nbIterations);
        }
        
        public static byte[] HashData(HashAlgorithm algorithm, byte[] data, int nbIterations)
        {
            return HashData(algorithm, data, 0, data.Length, nbIterations);
        }
        public static byte[] HashData(HashAlgorithm algorithm, byte[] data, int start, int length, int nbIterations)
        {
            for(int i = 0; i < nbIterations; ++i)
                data = algorithm.ComputeHash(data, start, length);
            return data;
        }
    }
}